# STD-Embedev

## Testing

To run unit tests on the code base make sure that the Google Test submodule is initialized and then run make inside the tests directory. 

```bash
git submodule update --init --recursive
cd tests
make
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
