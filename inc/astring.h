/**
 * @file astring.h
 * @author Brandon Miller - Embedev
 * @brief Header file for astring.cpp
 *
 * MIT License
 *
 * Copyright (c) 2019 Brandon Miller (Embedev)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#ifndef ASTRING_H_
#define ASTRING_H_

#include <common.h>

#ifdef __cplusplus
extern "C" {
#endif

void astring_null_terminate(char* array, uchar size);
void astring_clear(char* array, uchar size);
uchar astring_get_small_string_size_in_array(char* array);
void astring_copy_diff_sizes(char* dest_array, uchar dest_size, char* src_array, uchar src_size);

#ifdef __cplusplus
}
#endif


#endif /* ASTRING_H_ */
