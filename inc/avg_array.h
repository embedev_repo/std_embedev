/**
 * @file avg_array.h
 * @author Brandon Miller - Embedev
*/

#ifndef AVG_ARRAY_H_
#define AVG_ARRAY_H_

#include <common.h>



template <class T, uchar ASIZE>
class Avg_Array {
	private:
		T array[ASIZE];		
		uchar array_curr_index = 0;
		uchar spots_filled = 0;

	public:
		void clear();
		uchar count();
		void add(T value);
		//T get(uchar index);
		T get_avg();
};


template <class T, uchar ASIZE>
void Avg_Array<T,ASIZE>::clear() 
{
	memset(array, 0, ASIZE);
	array_curr_index = 0;
	spots_filled = 0;
}

template <class T, uchar ASIZE>
uchar Avg_Array<T,ASIZE>::count() 
{
	return spots_filled;
}

template <class T, uchar ASIZE>
void Avg_Array<T,ASIZE>::add(T value) 
{
	array[array_curr_index] = value;
	array_curr_index++;

	if (spots_filled < ASIZE) { spots_filled++; }
	

	if (array_curr_index >= ASIZE) { array_curr_index = 0; }
}

template <class T, uchar ASIZE>
T Avg_Array<T,ASIZE>::get_avg() 
{
	T result = 0;

	// Add up all numbers
	for (uchar i = 0; i < spots_filled; ++i) {
		result += array[i];
	}

	result /= spots_filled;
	return result;
}



#endif /* AVG_ARRAY_H_ */
