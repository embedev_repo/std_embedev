/**
 * @file byte.h
 * @author Brandon Miller - Embedev
*/

#ifndef BYTE_H_
#define BYTE_H_

#include <common.h>

#ifdef __cplusplus
extern "C" {
#endif
void byte_set_bit(uchar* byte, uchar index);
void byte_clear_bit(uchar* byte, uchar index);
void byte_reverse(uchar* b);
#ifdef __cplusplus
}
#endif

#endif /* BYTE_H_ */
