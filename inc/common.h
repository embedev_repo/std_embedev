/**
 * @file types.h
 * @author Brandon Miller - Embedev
 * @brief Common Types 
 *
 * MIT License
 *
 * Copyright (c) 2019 Brandon Miller (Embedev)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#ifndef COMMON_H_
#define COMMON_H_

#include <stdbool.h>



// Common bools
#define B_SUCCESS   true
#define B_FAIL      false

#define B_ENABLE    true
#define B_DISABLE   false

#define B_ENABLED   true
#define B_DISABLED  false

#define B_ON        true
#define B_OFF       false

// Common Macros
#define RET_IF_FALSE(x)       if (!x) { return; }
#define RET_FAIL_IF_FALSE(x)  if (!x) { return B_FAIL; }


// Common Unsigned Types:
typedef unsigned char            uchar;
typedef unsigned short int       ushort;
typedef unsigned int             uint;
typedef unsigned long int        ulong;

// Common Signed Types:
typedef signed char              schar;
typedef short int                sshort;
typedef int                      sint;
typedef long int                 slong;



#define LOBYTE(a) ((uchar)(a))
#define HIBYTE(a) ((uchar)((a) >> 8))



#endif /* COMMON_H_ */
