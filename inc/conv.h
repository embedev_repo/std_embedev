/**
 * @file conv.h
 * @author Brandon Miller - Embedev
*/

#ifndef CONV_H_
#define CONV_H_

#include <common.h>

void conv_two_uchar_to_ushort(uchar byte_low, uchar byte_high, ushort* var);

#endif /* CONV_H_ */
