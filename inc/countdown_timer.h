/**
 * @file timer.h
 * @author Brandon Miller - Embedev
*/

#ifndef TIMER_H_
#define TIMER_H_

#include <common.h>

class Countdown_Timer
{
	private:
		bool start_flag = false;
		bool end_flag = false;
		bool running = false;
		ulong period = 0; 
		ulong timer = 0;
	public:
		Countdown_Timer();
		Countdown_Timer(ulong timer_period);
		void start();
		void pause();
		void resume();
		void reset();
		void run();
		bool is_start();
		bool is_end();
		ulong get_timer();
		void change_period(ulong new_period);
};






#endif /* TIMER_H_ */
