/**
 * @file linked_list.h
 * @author Brandon Miller - Embedev
 * @brief Template for creating a linked list
 *
 * MIT License
 *
 * Copyright (c) 2019 Brandon Miller (Embedev)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#ifndef LINKED_LIST_H_
#define LINKED_LIST_H_

#include <common.h>


template <class T>
struct node
{
    T data;
    node<T> *next;
};

template <class T, ulong N>
class linked_list
{
	public:
        	linked_list();
        	~linked_list();
        	bool insert_at_end(T new_data);
		ulong size();
		T get_at_index(ulong index);
		T operator[](int index);
		bool is_empty();
		void clear();
		bool delete_at_index(ulong index);
		bool delete_first();
		bool delete_last();
		void move_to_index(ulong curr_index, ulong new_index);

    	private:
        	node<T> *head;
        	node<T> *tail;
		ulong list_size;
		ulong list_max_size;
};

template <class T, ulong N>
linked_list<T,N>::linked_list()
{
	head = NULL;
	tail = NULL;
	list_size = 0;
	list_max_size = N;
}

template <class T, ulong N>
ulong linked_list<T,N>::size()
{
	return list_size;
}

//template <class T, ulong N>
//bool move_after_index(ulong curr_index, ulong index_to_move_to);
//{
//	node<T> *node_ptr = head;
//
//	// Check to make sure list isn't empty
//	if (head == NULL) { return false; }
//
//	// Check to make sure index is valid
//	if (curr_index > list_size) { return false; }
//
//	// If only one node in list then delete that node
//	if (list_size == 1) { 
//		return false;
//	}
//
//
//	// Advance until the node right before one to delete
//	for(ulong i = 0; i < index; ++i) {
//		node_ptr = node_ptr->next;
//	}
//
//}

template <class T, ulong N>
bool linked_list<T,N>::delete_at_index(ulong index)
{
	node<T> *node_ptr = head;

	// Check to make sure list isn't empty
	if (head == NULL) { return false; }

	// Check to make sure index is valid
	if (index > list_size) { return false; }

	// If only one node in list then delete that node
	if (list_size == 1) { 
		clear();
		return true;
	}

	// At least two nodes
	if (index == 0) {  // If deleting the head node
		node_ptr = head;
		head = node_ptr->next;

		// Delete the node
		delete(node_ptr);

		// Subtact one from list size
		list_size--;

		return true;
	}

	// Start at head
	node_ptr = head;

	// Advance until the node right before one to delete
	for(ulong i = 1; i < index; ++i) {
		node_ptr = node_ptr->next;
	}

	// Check if node to delete is tail
	if (tail == node_ptr->next) {
		tail = node_ptr;
	}


	node<T> *delete_ptr = node_ptr->next;

	// Relink next
	node_ptr->next = delete_ptr->next;


	// Delete the node
	delete(delete_ptr);

	// Subtact one from list size
	list_size--;

	return true;
}

template <class T, ulong N>
bool linked_list<T,N>::is_empty()
{
	return list_size ? false : true;
}

template <class T, ulong N>
bool linked_list<T,N>::delete_first()
{
	return delete_at_index(0);
}

template <class T, ulong N>
bool linked_list<T,N>::delete_last()
{
	return delete_at_index(list_size-1);
}

template <class T, ulong N>
linked_list<T,N>::~linked_list()
{
	clear();
}

template <class T, ulong N>
void linked_list<T,N>::clear()
{
	node<T>* temp = head;
	while(temp != NULL)
	{
		temp = temp->next;
		delete(head);
		head = temp;
	}

	// Clear list state
	head = NULL;
	tail = NULL;
	list_size = 0;
}

template <class T, ulong N>
bool linked_list<T,N>::insert_at_end(T new_data)
{
	// First check if list can hold more
	if (list_size >= list_max_size) { return false; } 

	node<T> *new_node = new node<T>;
	new_node->data = new_data;
	new_node->next = NULL;

	if (head == NULL) {
		head = new_node;
		tail = new_node;
	}
	else {
		tail->next = new_node;
		tail = new_node;
	}

	// Increase size
	list_size++;

	return true;
}

template <class T, ulong N>
T linked_list<T,N>::get_at_index(ulong index)
{
	node<T> *node_ptr = head;

	if (index == 0 || index > list_size) { return node_ptr->data; }


	if (index != 0) {
		for(ulong i = 0; i < index; ++i) {
			node_ptr = node_ptr->next;
		}
	}

	return node_ptr->data;
}

template <class T, ulong N>
T linked_list<T,N>::operator[](int index) 
{
	return get_at_index(index);
}



#endif /* LINKED_LIST_H_ */
