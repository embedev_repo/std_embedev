/**
 * @file ring_buff.h
 * @author Brandon Miller - Embedev
 * @brief Template for creating a ring buffer
 *
 * MIT License
 *
 * Copyright (c) 2019 Brandon Miller (Embedev)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#ifndef RING_BUFF_H_
#define RING_BUFF_H_

#include <common.h>
#include <stddef.h>


template <class T>
class ring_buffer {
public:
	ring_buffer(size_t size) 
	{
		rbuff = new T[size];
		max_size = size;
	}

	void put(T item) 
	{
		if (full) { return; }

		rbuff[head] = item;

		head = (head + 1) % max_size;
		full = head == tail;
	}

	T get()	
	{
		if(is_empty())
		{
			return T();
		}

		//Read data and advance the tail (we now have a free space)
		auto val = rbuff[tail];
		full = false;
		tail = (tail + 1) % max_size;

		return val;
	}

	void clear() 
	{
		head = tail;
		full = false;
	}

	bool is_empty() 
	{
		return (!full && (head == tail));
	}

	bool is_full() 
	{
		return full;
	}

	size_t capacity() 
	{
		return max_size;
	}

	size_t size() 
	{

		size_t size = max_size;

		if(!full) {
			if(head >= tail) {
				size = head - tail;
			}
			else {
				size = max_size + head - tail;
			}
		}

		return size;
	}

private:
	T *rbuff;
	size_t head = 0;
	size_t tail = 0;
	size_t max_size;
	bool full = 0;
};


#endif /* RING_BUFF_H_ */
