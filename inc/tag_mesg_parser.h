/**
 * @file tag_mesg_parser.h
 * @author Brandon Miller - Embedev
 * @brief Header for tag_mesg_parser.c
 *
 * MIT License
 *
 * Copyright (c) 2019 Brandon Miller (Embedev)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#ifndef EMBEDEV_TAG_MESG_PARSER_H_
#define EMBEDEV_TAG_MESG_PARSER_H_

#include <common.h>

class Tag_Mesg_Parser {
	public:
		Tag_Mesg_Parser();
		~Tag_Mesg_Parser();
		//Tag_Mesg_Parser(char* mesg_str, int mesg_size);
		bool load_mesg_str(char* mesg_str, int mesg_size);
		bool load_tag_str(char* tag_str, int tag_size);
		bool get_byte_at_offset(uchar* val, uchar offset);
		//bool get_val_at_offset(ulong* val, uchar offset);
		bool get_tag_byte(uchar* val);
		bool load_tag_and_get_tag_byte(char* tag_str, int size, uchar* val);
		bool get_tag_group(char* group_str, uchar group_size);
		bool get_tag_group_part(char* group_part_str, uchar group_part_size, uchar index);
		bool get_tag_group_part_as_int(int* grout_part_int, uchar index);
		bool compare_tag_at_offset(uchar offset);
	private:
		char * mesg_ptr = nullptr;
		ulong  mesg_size = 0;
		char * tag_ptr = nullptr;
		ulong  tag_size = 0;
		char * find_tag_start();
		char * find_val_start(char* start_search_ptr);
		char * find_group_start(char* start_search_ptr);
		char * find_group_end(char* start_search_ptr);
		bool valid_hex_char(char c);
		void copy_str_until_end_or_size(char* start, char* end, char* buff, uchar size);
		void copy_str_until_null_or_size(char* start, char* buff, uchar size);
};


#endif /* EMBEDEV_TAG_MESG_PARSER_H_ */
