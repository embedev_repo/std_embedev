/**
 * @file astring.c
 * @author Brandon Miller - Embedev
 * @brief Helper functions to work with arrays being used as strings
 *
 * MIT License
 *
 * Copyright (c) 2019 Brandon Miller (Embedev)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/


#include "astring.h"

void astring_null_terminate(char* array, uchar size) 
{ 
	// Check input
	if (array == nullptr) { return; }
	if (size == 0) { return; }

	array[size-1] = '\0'; 
}


void astring_clear(char* array, uchar size) 
{ 
	// Check input
	if (array == nullptr) { return; }
	if (size == 0) { return; }

	for (uchar i = 0; i < size; i++) {
		array[i] = '\0';	
	}
}

uchar astring_get_small_string_size_in_array(char* array)
{
	uchar index = 0;
	// Check and prep input
	if (array == nullptr) { return 0; }

	while(array[index] != '\0') { index++; }

	return index;

}

void astring_copy_diff_sizes(char* dest_array, uchar dest_size, char* src_array, uchar src_size)
{
	// Check input and state
	if (dest_array == nullptr || src_array == nullptr) { return; }
	if (dest_size == 0 || src_size == 0) { return; }

	// Get size
	uchar min_size = dest_size < src_size ? dest_size : src_size;

	// Copy array
	for (uchar i = 0; i < min_size; ++i) {
		dest_array[i] = src_array[i];
		
	}
}



