/**
 * @file byte.cpp
 * @author Brandon Miller - Embedev
 * @brief Simple byte manipulation functions
*/

#include <byte.h>

void byte_set_bit(uchar* byte, uchar index)
{
	// Check input
	if (byte == nullptr) { return; }
	if (index >= 8) { return; }

	uchar bit = 0x01 << index;
	*byte |= bit;  
}

void byte_clear_bit(uchar* byte, uchar index)
{
	// Check input
	if (byte == nullptr) { return; }
	if (index >= 8) { return; }

	uchar bit = 0x01 << index;
	bit = ~bit;
	*byte &= bit;  
}

void byte_reverse(uchar* byte)
{
   *byte = (*byte & 0xF0) >> 4 | (*byte & 0x0F) << 4;
   *byte = (*byte & 0xCC) >> 2 | (*byte & 0x33) << 2;
   *byte = (*byte & 0xAA) >> 1 | (*byte & 0x55) << 1;
}




