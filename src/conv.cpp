/**
 * @file conv.cpp
 * @author Brandon Miller - Embedev
 * @brief This file...
*/

#include <conv.h>

void conv_two_uchar_to_ushort(uchar byte_low, uchar byte_high, ushort* var)
{
	if (var == nullptr) { return; }

	ushort result = (byte_high << 8);
	result |= byte_low;
	*var = result;
}


