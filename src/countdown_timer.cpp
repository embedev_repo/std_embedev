/**
 * @file timer.cpp
 * @author Brandon Miller - Embedev
 * @brief A general countdown timer 
*/

#include <countdown_timer.h>

/*!
 * @brief Basic constructor
*/
Countdown_Timer::Countdown_Timer()	
{
	period = 0;
	reset();
}

/*!
 * @brief Basic constructor for setting period
*/
Countdown_Timer::Countdown_Timer(ulong timer_period)	
{
	period = timer_period;
	reset();
}


/*!
 * @brief Starts the timer from the beginning and resets all values
*/
void Countdown_Timer::start()
{
	reset();

	running = true;
	start_flag = true;
}

/*!
 * @brief Pause the timer
*/
void Countdown_Timer::pause()
{
	running = false;
}

/*!
 * @brief Resumes a timer if paused
*/
void Countdown_Timer::resume()
{
	running = true;
}

/*!
 * @brief Resets the timer back to init settings but does not start
*/
void Countdown_Timer::reset()
{
	start_flag = false;
	end_flag = false;
	running = false;

	timer = period;
}


/*!
 * @brief Tell if timer has been started. Only true before first run.
 * @retvar bool True - start just triggered, False - No start 
*/
bool Countdown_Timer::is_start()
{
	return start_flag;
}

/*!
 * @brief Tell if timer has ended.
 * @retvar bool True - Ended, False - No end 
*/
bool Countdown_Timer::is_end()
{
	return end_flag;
}


/*!
 * @brief Runs the timer by counting down.  Doesn't run if paused. Clears start flag.
*/
void Countdown_Timer::run()
{
	// Don't run if not running or paused
	if (!running) { return; }

	if (timer > 0) { timer--; }
	if (timer == 0) { end_flag = true; }

	// Clear out start flag after first go
	if (start_flag) {
		start_flag = false;
	}
}


/*!
 * @brief Gets current timer value
 * @retvar ulong Timer value
*/
ulong Countdown_Timer::get_timer()
{
	return timer;
}



/*!
 * @brief Change the timer period and reset
 * @param[in] ulong New timer period
 * @retvar void
*/
void Countdown_Timer::change_period(ulong new_period)
{
	period = new_period;
	reset();
}
