/**
 * @file tag_mesg_parser.c
 * @author Brandon Miller - Embedev
 * @brief A simple text based message protocol. Good for uart comms.
 *
 * MIT License
 *
 * Copyright (c) 2019 Brandon Miller (Embedev)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "tag_mesg_parser.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

Tag_Mesg_Parser::Tag_Mesg_Parser() {}

Tag_Mesg_Parser::~Tag_Mesg_Parser() 
{
	mesg_ptr =  nullptr;
	mesg_size = 0;
	tag_ptr =  nullptr;
	tag_size = 0;
}

//Tag_Mesg_Parser(char* mesg_str, int mesg_size)
//{
//	load_mesg_str(mesg_str, size); 
//}

bool Tag_Mesg_Parser::load_mesg_str(char* mesg_str, int size) 
{
	// Check input
	if (mesg_str ==  nullptr) { return false; }
	if (size == 0) { return false; }
	if (mesg_str[size-1] != '\0') { return false; }

	mesg_ptr = mesg_str;
	mesg_size = size;
	return true;
}


bool Tag_Mesg_Parser::load_tag_str(char* tag_str, int size)
{
	// Check input
	if (tag_str ==  nullptr) { return false; }
	if (size == 0) { return false; }
	if (tag_str[size-1] != '\0') { return false; }

	tag_ptr = tag_str;
	tag_size = size;
	return true;
}

bool Tag_Mesg_Parser::get_byte_at_offset(uchar* val, uchar offset)
{
	char value_str[] = "00";

	// Check input
	if (mesg_size < (sizeof(value_str) + offset)) { return false; }
	//TODO: add check if chars are valid hex value

	// Offset message pointer
	char* ptr = mesg_ptr;
	ptr = ptr + offset;  

	// Check hex value
	if (!valid_hex_char(ptr[0])) { return false; }
	if (!valid_hex_char(ptr[1])) { return false; }

	// Copy mesg to modify
      	strncpy(value_str, ptr, sizeof(value_str)-1);	

	*val = uchar(strtol(value_str,  nullptr, 16));

	return true;
}
//bool Tag_Mesg_Parser::get_val_at_offset(ulong* val, uchar offset)
//{
//	char value_str[] = "00";
//
//	// Check input
//	//if (mesg_size < (sizeof(value_str) + offset)) { return false; }
//	//TODO: add check if chars are valid hex value
//
//	// Offset message pointer
//	char* ptr = mesg_ptr;
//	ptr = ptr + offset;  
//
//	// Check hex value
//	//if (!valid_hex_char(ptr[0])) { return false; }
//	//if (!valid_hex_char(ptr[1])) { return false; }
//
//	// Copy mesg to modify
//      	strncpy(value_str, ptr, sizeof(value_str)-1);	
//
//	*val = uchar(strtol(value_str,  nullptr, 16));
//
//	return true;
//}


bool Tag_Mesg_Parser::get_tag_byte(uchar* val)
{
	// Check Input
	if (val ==  nullptr) { return false; }

	// Check that the tag exists
	char* tag_ptr = find_tag_start();
	if (tag_ptr ==  nullptr) { return false; }

	// Check that value exists
	char* val_ptr = find_val_start(tag_ptr);
	if (val_ptr ==  nullptr || *val_ptr == '\0') { return false; }

	// Check hex value
	if (!valid_hex_char(val_ptr[0])) { return false; }
	if (!valid_hex_char(val_ptr[1])) { return false; }

	// Save value
	*val = uchar(strtol(val_ptr,  nullptr, 16));

	return true;
}

bool Tag_Mesg_Parser::load_tag_and_get_tag_byte(char* tag_str, int size, uchar* val)
{
	bool ret = false;

	// Load tag string
	ret = load_tag_str(tag_str, size);
	if (!ret) { return ret; }

	// Try to find and get func value
	ret = get_tag_byte(val);
	return ret; 
}

bool Tag_Mesg_Parser::get_tag_group(char* group_str, uchar group_size)
{
	// Check Input
	if (group_str ==  nullptr) { return false; }
	//if (group_size != VINE_MESG_MAX_GROUP_STR_SIZE) { return false; }

	// Check that the tag exists
	char* tag_ptr = find_tag_start();
	if (tag_ptr ==  nullptr) { return false; }

	// Check that group start exists
	char* group_start_ptr = find_group_start(tag_ptr);
	if (group_start_ptr ==  nullptr) { return false; }

	// Check that group end exists
	char* group_end_ptr = find_group_end(group_start_ptr);
	if (group_end_ptr ==  nullptr) { return false; }

	// Check if anything between brackets
	if (group_start_ptr == group_end_ptr) { return false; }

	// Copy over until end
	copy_str_until_end_or_size(group_start_ptr, group_end_ptr, group_str, group_size);

	return true;
}

bool Tag_Mesg_Parser::get_tag_group_part(char* group_part_str, uchar group_part_size, uchar index)
{
	char group_delim[] = ",";

	// Check input
	if (group_part_str ==  nullptr) { return false; }
	if (group_part_size == 0) { return false; }


	// Get group string
	char group_str[100]; // Limit to 100 chars
	bool ret = get_tag_group(group_str, sizeof(group_str));
	if (!ret) { return false; }


	uchar count = 0;
	char *ptr = strtok(group_str, group_delim);

	while(ptr !=  nullptr && count < index) {
		ptr = strtok( nullptr, group_delim);
		count++;
	}

	// Check pointer
	if (ptr ==  nullptr) { return false; }

	// Copy over part
	copy_str_until_null_or_size(ptr, group_part_str, group_part_size);

	return true;

}

bool Tag_Mesg_Parser::get_tag_group_part_as_int(int* group_part_int, uchar index)
{
	// Check input
	if (group_part_int ==  nullptr) { return false; }

	char group_part_str[100]; // Limit to 100 chars
	bool ret = get_tag_group_part(group_part_str, sizeof(group_part_str), index);
	if (!ret) { return false; }

	*group_part_int = strtol(group_part_str,  nullptr, 10);

	return true;
}

bool Tag_Mesg_Parser::compare_tag_at_offset(uchar offset)
{
	char * mesg_check_ptr = mesg_ptr;
	mesg_check_ptr += offset;
	char * tag_check_ptr = tag_ptr;

	for (uchar i = 0; i < tag_size-1; ++i) {
		if (mesg_check_ptr[i] != tag_check_ptr[i]) { return false; }	
	}

	return true;
}






//#############################################################################
// Private Functions:
//#############################################################################

char* Tag_Mesg_Parser::find_tag_start()
{
	return strstr(mesg_ptr, tag_ptr);
}

char* Tag_Mesg_Parser::find_val_start(char* start_search_ptr)
{
	char* ptr = strchr(start_search_ptr, '=');
	if (ptr !=  nullptr) { return ++ptr; }
	return ptr;
}

char* Tag_Mesg_Parser::find_group_start(char* start_search_ptr)
{
	char* ptr =  strchr(start_search_ptr, '(');
	if (ptr !=  nullptr) { return ++ptr; }
	return ptr; 
}

char* Tag_Mesg_Parser::find_group_end(char* start_search_ptr)
{
	return strchr(start_search_ptr, ')');
}

bool Tag_Mesg_Parser::valid_hex_char(char c)
{
	if (c == '\0') { return false; }
	if (c >= '0' && c <= '9') { return true; }
	if (c >= 'a' && c <= 'f') { return true; }
	if (c >= 'A' && c <= 'F') { return true; }
	return false;
}


void Tag_Mesg_Parser::copy_str_until_end_or_size(char* start, char* end, char* buff, uchar size)
{
	uchar i = 0;

	// Check input
	if (buff ==  nullptr) { return; }
	if (size == 0) { return; }
	if (start == end) { goto exit; }

	for (i = 0; i < (size-1); ++i) {
		buff[i] = *start++;
		if (start == end) { goto exit; }
	}

	exit:
	buff[++i] = '\0';
	buff[size-1] = '\0'; // Just in case
}

void Tag_Mesg_Parser::copy_str_until_null_or_size(char* start, char* buff, uchar size)
{
	char*  nullptr_ptr = strchr(start, '\0');

	if ( nullptr_ptr ==  nullptr) { return; }

	copy_str_until_end_or_size(start,  nullptr_ptr, buff, size);
}

