#include <gtest/gtest.h>

#include <common.h>
#include <avg_array.h>


// Test fixture class
class tf_avg_array : public ::testing::Test {
	protected:
		//FooTest() { }
		//~FooTest() override { }

		void SetUp() override {
		}

		void TearDown() override {
		}
};


namespace {


TEST_F(tf_avg_array, array_size_zero) {

	// Create
	Avg_Array<uchar, 2> avg_array;

	EXPECT_EQ(0, avg_array.count());
}

TEST_F(tf_avg_array, add_one_item) {

	// Create
	Avg_Array<uchar, 2> avg_array;

	avg_array.add(8);
	EXPECT_EQ(1, avg_array.count());
}

TEST_F(tf_avg_array, add_one_item_with_avg) {

	// Create
	Avg_Array<uchar, 2> avg_array;

	avg_array.add(10);
	EXPECT_EQ(1, avg_array.count());
	EXPECT_EQ(10, avg_array.get_avg());
}

TEST_F(tf_avg_array, add_two_item_with_avg) {

	// Create
	Avg_Array<uchar, 4> avg_array;

	avg_array.add(5);
	avg_array.add(5);
	EXPECT_EQ(2, avg_array.count());
	EXPECT_EQ(5, avg_array.get_avg());
}

TEST_F(tf_avg_array, add_four_item_overflow) {

	// Create
	Avg_Array<uchar, 3> avg_array;

	avg_array.add(1);
	avg_array.add(20);
	avg_array.add(20);
	avg_array.add(20);
	EXPECT_EQ(3, avg_array.count());
	EXPECT_EQ(20, avg_array.get_avg());
}

}  // namespace
