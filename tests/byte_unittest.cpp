
#include <gtest/gtest.h>
#include <byte.h>

namespace {

TEST(byte, bad_input_null) {
	byte_set_bit(NULL, 0);
	byte_clear_bit(NULL, 0);
}

TEST(byte, bad_input_index) {
	uchar byte = 0xFF;
	byte_set_bit(&byte, 32);
	EXPECT_EQ(0xFF, byte);
	byte_clear_bit(&byte, 73);
	EXPECT_EQ(0xFF, byte);
}

TEST(byte, set_check_all_positions) {
	uchar byte = 0x00;
	uchar index = 0;
	uchar val = 0x01;

	for (uchar i = 0; i < 8; ++i) {
		byte = 0x00;
		index = i;
		val = 0x01;
		byte_set_bit(&byte, index);
		EXPECT_EQ(val << index, byte);
	}
}

TEST(byte, clear_check_all_positions) {
	uchar byte = 0xFF;
	uchar index = 0;
	uchar val = 0x01;

	for (uchar i = 0; i < 8; ++i) {
		byte = 0xFF;
		index = i;
		val = 0x01;
		byte_clear_bit(&byte, index);
		val = ~(val << index);
		EXPECT_EQ(val, byte);
	}
}

TEST(byte, reverse) {
	uchar byte = 0b11010010;
	byte_reverse(&byte);
	EXPECT_EQ(0b01001011, byte);

}

}  // namespace

