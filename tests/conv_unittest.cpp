#include <gtest/gtest.h>
#include <conv.h>

namespace {

TEST(conv, uchar_to_short_nominal) {
	uchar byte1 = 0x12;
	uchar byte2 = 0x34;
	ushort result = 0x3412;
	ushort ret = 0;
	conv_two_uchar_to_ushort(byte1, byte2, &ret);
	EXPECT_EQ(result, ret);
}


}  // namespace


