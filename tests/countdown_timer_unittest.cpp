
#include <gtest/gtest.h>
#include <countdown_timer.h>

namespace {

TEST(countdown_timer_unittest, create_new) {
	Countdown_Timer Timer1;
	Countdown_Timer Timer2(5);
}

TEST(countdown_timer_unittest, check_period) {
	Countdown_Timer timer1;
	Countdown_Timer timer2(5);

	EXPECT_EQ(0u, timer1.get_timer());
	EXPECT_EQ(5u, timer2.get_timer());
}

TEST(countdown_timer_unittest, change_period) {
	Countdown_Timer timer1;
	EXPECT_EQ(0u, timer1.get_timer());

	timer1.change_period(10);
	EXPECT_EQ(10u, timer1.get_timer());
}

TEST(countdown_timer_unittest, check_start) {
	Countdown_Timer timer1(5);
	EXPECT_EQ(false, timer1.is_start());

	timer1.start();
	EXPECT_EQ(true, timer1.is_start());

	// Clears out first start
	timer1.run();
	EXPECT_EQ(false, timer1.is_start());
}

TEST(countdown_timer_unittest, check_end) {
	ulong period = 5;
	Countdown_Timer timer1(period);
	EXPECT_EQ(false, timer1.is_end());

	timer1.start();
	EXPECT_EQ(period, timer1.get_timer());
	EXPECT_EQ(false, timer1.is_end());

	while (period > 1) {
		timer1.run();
		period--;
		EXPECT_EQ(period, timer1.get_timer());
		EXPECT_EQ(false, timer1.is_end());
	}

	timer1.run();
	period--;
	EXPECT_EQ(true, timer1.is_end());
}

TEST(countdown_timer_unittest, check_pause) {
	ulong period = 5;
	Countdown_Timer timer1(period);

	timer1.start();
	timer1.run();
	timer1.run();
	EXPECT_EQ(3u, timer1.get_timer());
	timer1.pause();

	timer1.run();
	EXPECT_EQ(3u, timer1.get_timer());
	timer1.run();
	EXPECT_EQ(3u, timer1.get_timer());
	timer1.run();
	EXPECT_EQ(3u, timer1.get_timer());
}

TEST(countdown_timer_unittest, check_resume) {
	ulong period = 5;
	Countdown_Timer timer1(period);

	timer1.start();
	timer1.run();
	timer1.run();
	EXPECT_EQ(3u, timer1.get_timer());
	timer1.pause();

	timer1.run();
	timer1.run();
	timer1.run();

	timer1.resume();

	timer1.run();
	EXPECT_EQ(2u, timer1.get_timer());
	timer1.run();
	EXPECT_EQ(1u, timer1.get_timer());
	timer1.run();
	EXPECT_EQ(0u, timer1.get_timer());
}

TEST(countdown_timer_unittest, check_reset) {
	ulong period = 3;
	Countdown_Timer timer1(period);

	timer1.start();
	EXPECT_EQ(true, timer1.is_start());
	timer1.reset();
	EXPECT_EQ(false, timer1.is_start());

	timer1.start();
	timer1.run();
	timer1.run();
	timer1.reset();
	EXPECT_EQ(period, timer1.get_timer());

	timer1.start();
	timer1.run();
	timer1.run();
	timer1.pause();
	timer1.reset();

	timer1.start();
	timer1.run();
	timer1.run();
	timer1.run();
	EXPECT_EQ(true, timer1.is_end());
	timer1.reset();
	EXPECT_EQ(false, timer1.is_end());
}

TEST(countdown_timer_unittest, check_run_does_not_countdown_until_start) {
	ulong period = 10;
	Countdown_Timer timer1(period);

	timer1.run();
	EXPECT_EQ(period, timer1.get_timer());
	timer1.run();
	EXPECT_EQ(period, timer1.get_timer());
	timer1.run();
	EXPECT_EQ(period, timer1.get_timer());

}


}  // namespace


