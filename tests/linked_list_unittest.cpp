#include <gtest/gtest.h>

#include <common.h>
#include <linked_list.h>

struct test_struct {
	uchar total;
};

struct test_struct stest1;	
struct test_struct stest2;	
struct test_struct stest3;	
struct test_struct stest4;	

// Test fixture class
class tf_linked_list : public ::testing::Test {
	protected:
		//FooTest() { }
		//~FooTest() override { }

		void SetUp() override {
			stest1.total = 1;
			stest2.total = 2;
			stest3.total = 3;
			stest4.total = 4;
		}

		void TearDown() override {
		}
};


namespace {


//TEST_F(tf_linked_list, list_size_zero) {
//	std_embedev::linked_list<struct test_struct,4> list;
//	EXPECT_EQ(0u, list.size());
//}
//
//TEST_F(tf_linked_list, add_item_to_end) {
//	std_embedev::linked_list<struct test_struct,4> list;
//	list.insert_at_end(stest1);
//	EXPECT_EQ(1u, list.size());
//	EXPECT_EQ(1, list[0].total);
//}
//
//TEST_F(tf_linked_list, add_two_items_to_end) {
//	std_embedev::linked_list<struct test_struct,4> list;
//	list.insert_at_end(stest1);
//	list.insert_at_end(stest2);
//	EXPECT_EQ(2u, list.size());
//	EXPECT_EQ(1, list[0].total);
//	EXPECT_EQ(2, list[1].total);
//}
//
////TEST_F(tf_linked_list, get_at_index_empty_list) {
//	//std_embedev::linked_list<struct test_struct,4> list;
//	//list.get_at_index(0);
//	//EXPECT_EQ(NULL, list.get_at_index(0));
////}
//
//TEST_F(tf_linked_list, is_list_empty) {
//	std_embedev::linked_list<struct test_struct,4> list;
//	EXPECT_EQ(true, list.is_empty());
//	list.insert_at_end(stest1);
//	EXPECT_EQ(false, list.is_empty());
//}
//
//TEST_F(tf_linked_list, delete_one_node_from_list) {
//	std_embedev::linked_list<struct test_struct,4> list;
//	list.insert_at_end(stest1);
//	EXPECT_EQ(1u, list.size());
//	EXPECT_EQ(1, list[0].total);
//
//	// Delete node
//	list.delete_at_index(0);
//	EXPECT_EQ(0u, list.size());
//}
//
//TEST_F(tf_linked_list, delete_first_node_from_two_node_list) {
//	std_embedev::linked_list<struct test_struct,4> list;
//	list.insert_at_end(stest1);
//	list.insert_at_end(stest2);
//	EXPECT_EQ(2u, list.size());
//	EXPECT_EQ(1, list[0].total);
//	EXPECT_EQ(2, list[1].total);
//
//	// Delete node
//	list.delete_at_index(0);
//	EXPECT_EQ(1u, list.size());
//	EXPECT_EQ(2, list[0].total);
//}
//
//TEST_F(tf_linked_list, delete_second_node_from_two_node_list) {
//	std_embedev::linked_list<struct test_struct,4> list;
//	list.insert_at_end(stest1);
//	list.insert_at_end(stest2);
//	EXPECT_EQ(2u, list.size());
//	EXPECT_EQ(1, list[0].total);
//	EXPECT_EQ(2, list[1].total);
//
//	// Delete node
//	list.delete_at_index(1);
//	EXPECT_EQ(1u, list.size());
//	EXPECT_EQ(1, list[0].total);
//}
//
//TEST_F(tf_linked_list, delete_first_node_from_three_node_list) {
//	std_embedev::linked_list<struct test_struct,4> list;
//	list.insert_at_end(stest1);
//	list.insert_at_end(stest2);
//	list.insert_at_end(stest3);
//	EXPECT_EQ(3u, list.size());
//	EXPECT_EQ(1, list[0].total);
//	EXPECT_EQ(2, list[1].total);
//	EXPECT_EQ(3, list[2].total);
//
//	// Delete node
//	list.delete_at_index(0);
//	EXPECT_EQ(2u, list.size());
//	EXPECT_EQ(2, list[0].total);
//	EXPECT_EQ(3, list[1].total);
//}
//
//TEST_F(tf_linked_list, delete_second_node_from_three_node_list) {
//	std_embedev::linked_list<struct test_struct,4> list;
//	list.insert_at_end(stest1);
//	list.insert_at_end(stest2);
//	list.insert_at_end(stest3);
//	EXPECT_EQ(3u, list.size());
//	EXPECT_EQ(1, list[0].total);
//	EXPECT_EQ(2, list[1].total);
//	EXPECT_EQ(3, list[2].total);
//
//	// Delete node
//	list.delete_at_index(1);
//	EXPECT_EQ(2u, list.size());
//	EXPECT_EQ(1, list[0].total);
//	EXPECT_EQ(3, list[1].total);
//}
//
//TEST_F(tf_linked_list, delete_third_node_from_three_node_list) {
//	std_embedev::linked_list<struct test_struct,4> list;
//	list.insert_at_end(stest1);
//	list.insert_at_end(stest2);
//	list.insert_at_end(stest3);
//	EXPECT_EQ(3u, list.size());
//	EXPECT_EQ(1, list[0].total);
//	EXPECT_EQ(2, list[1].total);
//	EXPECT_EQ(3, list[2].total);
//
//	// Delete node
//	list.delete_at_index(2);
//	EXPECT_EQ(2u, list.size());
//	EXPECT_EQ(1, list[0].total);
//	EXPECT_EQ(2, list[1].total);
//}
//
//TEST_F(tf_linked_list, delete_one_check_tail) {
//	std_embedev::linked_list<struct test_struct,4> list;
//	list.insert_at_end(stest1);
//	EXPECT_EQ(1u, list.size());
//	EXPECT_EQ(1, list[0].total);
//
//	// Delete node
//	list.delete_at_index(0);
//	EXPECT_EQ(0u, list.size());
//
//	// Add end which uses tail
//	list.insert_at_end(stest2);
//
//	// Check
//	EXPECT_EQ(1u, list.size());
//	EXPECT_EQ(2, list[0].total);
//}
//
//TEST_F(tf_linked_list, delete_third_node_check_tail) {
//	std_embedev::linked_list<struct test_struct,4> list;
//	list.insert_at_end(stest1);
//	list.insert_at_end(stest2);
//	list.insert_at_end(stest3);
//	EXPECT_EQ(3u, list.size());
//	EXPECT_EQ(1, list[0].total);
//	EXPECT_EQ(2, list[1].total);
//	EXPECT_EQ(3, list[2].total);
//
//	// Delete node
//	list.delete_at_index(2);
//
//	// Check
//	EXPECT_EQ(2u, list.size());
//	EXPECT_EQ(1, list[0].total);
//	EXPECT_EQ(2, list[1].total);
//
//	// Add end which uses tail
//	list.insert_at_end(stest4);
//
//	// Check
//	EXPECT_EQ(3u, list.size());
//	//EXPECT_EQ(1, list[0].total);
//	//EXPECT_EQ(2, list[1].total);
//	//EXPECT_EQ(4, list[2].total);
//}



}  // namespace
