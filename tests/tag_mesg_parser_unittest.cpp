#include <gtest/gtest.h>

#include <common.h>
#include <tag_mesg_parser.h>

Tag_Mesg_Parser tag_parser;

// Test fixture class
class tf_tag_mesg_parser : public ::testing::Test {
	protected:
		//FooTest() { }
		//~FooTest() override { }

		void SetUp() override {
		}

		void TearDown() override {
		}
};


namespace {


TEST_F(tf_tag_mesg_parser, get_byte_at_offset_no_str) {
	char mesg_str[] = "";
	uchar value = 5;
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	bool ret =  tag_parser.get_byte_at_offset(&value, 0);
	EXPECT_EQ(false, ret);
	EXPECT_EQ(5, value);
}

TEST_F(tf_tag_mesg_parser, get_byte_at_offset_sort_str) {
	char mesg_str[] = "1";
	uchar value = 5;
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	bool ret =  tag_parser.get_byte_at_offset(&value, 0);
	EXPECT_EQ(false, ret);
	EXPECT_EQ(5, value);
}

TEST_F(tf_tag_mesg_parser, get_byte_at_offset_correct_1) {
	char mesg_str[] = "04";
	uchar value = 5;
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	bool ret =  tag_parser.get_byte_at_offset(&value, 0);
	EXPECT_EQ(true, ret);
	EXPECT_EQ(4, value);
}

TEST_F(tf_tag_mesg_parser, get_byte_at_offset_correct_2) {
	char mesg_str[] = "ba";
	uchar value = 5;
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	bool ret =  tag_parser.get_byte_at_offset(&value, 0);
	EXPECT_EQ(true, ret);
	EXPECT_EQ(186, value);
}

TEST_F(tf_tag_mesg_parser, get_byte_at_offset_garbage) {
	char mesg_str[] = "xz;aslkdj";
	uchar value = 5;
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	bool ret =  tag_parser.get_byte_at_offset(&value, 0);
	EXPECT_EQ(false, ret);
	EXPECT_EQ(5, value);
}

TEST_F(tf_tag_mesg_parser, get_byte_at_offset_with_offset) {
	char mesg_str[] = "02:06";
	uchar value = 5;
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	bool ret =  tag_parser.get_byte_at_offset(&value, 3);
	EXPECT_EQ(true, ret);
	EXPECT_EQ(6, value);
}

TEST_F(tf_tag_mesg_parser, get_byte_at_offset_with_offset_past) {
	char mesg_str[] = "02:06";
	uchar value = 5;
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	bool ret =  tag_parser.get_byte_at_offset(&value, 4);
	EXPECT_EQ(false, ret);
	EXPECT_EQ(5, value);
}

TEST_F(tf_tag_mesg_parser, get_tag_byte_missing_tag) {
	char mesg_str[] = "02:06";
	char tag_str[] = "CAT";
	uchar value = 5;
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_byte(&value);
	EXPECT_EQ(false, ret);
	EXPECT_EQ(5, value);
}

TEST_F(tf_tag_mesg_parser, get_tag_byte_tag_no_value_1) {
	char mesg_str[] = "02:06:CAT";
	char tag_str[] = "CAT";
	uchar value = 5;
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_byte(&value);
	EXPECT_EQ(false, ret);
	EXPECT_EQ(5, value);
}

TEST_F(tf_tag_mesg_parser, get_tag_byte_tag_no_value_2) {
	char mesg_str[] = "02:06:CAT=";
	char tag_str[] = "CAT";
	uchar value = 5;
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_byte(&value);
	EXPECT_EQ(false, ret);
	EXPECT_EQ(5, value);
}

TEST_F(tf_tag_mesg_parser, get_tag_byte_tag_no_value_3) {
	char mesg_str[] = "02:06:CAT=3";
	char tag_str[] = "CAT";
	uchar value = 5;
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_byte(&value);
	EXPECT_EQ(false, ret);
	EXPECT_EQ(5, value);
}

TEST_F(tf_tag_mesg_parser, get_tag_byte_correct) {
	char mesg_str[] = "02:06:CAT=32";
	char tag_str[] = "CAT";
	uchar value = 5;
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_byte(&value);
	EXPECT_EQ(true, ret);
	EXPECT_EQ(50, value);
}

TEST_F(tf_tag_mesg_parser, get_tag_byte_garbage) {
	char mesg_str[] = "02:06:CAT=r,,.";
	char tag_str[] = "CAT";
	uchar value = 5;
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_byte(&value);
	EXPECT_EQ(false, ret);
	EXPECT_EQ(5, value);
}

TEST_F(tf_tag_mesg_parser, get_tag_byte_extra) {
	char mesg_str[] = "02:06:CAT=07,34";
	char tag_str[] = "CAT";
	uchar value = 5;
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_byte(&value);
	EXPECT_EQ(true, ret);
	EXPECT_EQ(7, value);
}

TEST_F(tf_tag_mesg_parser, get_tag_group_missing_1) {
	char mesg_str[] = "02:06:AT=(34";
	char tag_str[] = "CAT";
	char group_str[10];
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_group(group_str, sizeof(group_str));
	EXPECT_EQ(false, ret);
}

TEST_F(tf_tag_mesg_parser, get_tag_group_missing_2) {
	char mesg_str[] = "02:06:CAT=";
	char tag_str[] = "CAT";
	char group_str[10];
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_group(group_str, sizeof(group_str));
	EXPECT_EQ(false, ret);
}

TEST_F(tf_tag_mesg_parser, get_tag_group_missing_3) {
	char mesg_str[] = "02:06:CAT=(";
	char tag_str[] = "CAT";
	char group_str[10];
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_group(group_str, sizeof(group_str));
	EXPECT_EQ(false, ret);
}

TEST_F(tf_tag_mesg_parser, get_tag_group_missing_4) {
	char mesg_str[] = "02:06:CAT=()";
	char tag_str[] = "CAT";
	char group_str[10];
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_group(group_str, sizeof(group_str));
	EXPECT_EQ(false, ret);
}

TEST_F(tf_tag_mesg_parser, get_tag_group_correct_num) {
	char mesg_str[] = "02:06:CAT=(34)";
	char tag_str[] = "CAT";
	char group_str[10];
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_group(group_str, sizeof(group_str));
	EXPECT_EQ(true, ret);
        EXPECT_EQ('3',  group_str[0]);
        EXPECT_EQ('4',  group_str[1]);
        EXPECT_EQ('\0', group_str[2]);
}

TEST_F(tf_tag_mesg_parser, get_tag_group_correct_str) {
	char mesg_str[] = "02:06:CAT=('34')";
	char tag_str[] = "CAT";
	char group_str[10];
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_group(group_str, sizeof(group_str));
	EXPECT_EQ(true, ret);
        EXPECT_EQ('\'',  group_str[0]);
        EXPECT_EQ('3',   group_str[1]);
        EXPECT_EQ('4',   group_str[2]);
        EXPECT_EQ('\'',  group_str[3]);
        EXPECT_EQ('\0',  group_str[4]);
}

TEST_F(tf_tag_mesg_parser, get_tag_group_correct_no_start_bracket) {
	char mesg_str[] = "02:06:CAT='34')";
	char tag_str[] = "CAT";
	char group_str[10];
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_group(group_str, sizeof(group_str));
	EXPECT_EQ(false, ret);
}

TEST_F(tf_tag_mesg_parser, get_tag_group_correct_no_end_bracket) {
	char mesg_str[] = "02:06:CAT=('34'";
	char tag_str[] = "CAT";
	char group_str[10];
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_group(group_str, sizeof(group_str));
	EXPECT_EQ(false, ret);
}

TEST_F(tf_tag_mesg_parser, get_tag_group_correct_multi) {
	char mesg_str[] = "02:06:CAT=('34',148)";
	char tag_str[] = "CAT";
	char group_str[10];
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_group(group_str, sizeof(group_str));
	EXPECT_EQ(true, ret);
        EXPECT_EQ('\'',  group_str[0]);
        EXPECT_EQ('3',   group_str[1]);
        EXPECT_EQ('4',   group_str[2]);
        EXPECT_EQ('\'',  group_str[3]);
        EXPECT_EQ(',',   group_str[4]);
        EXPECT_EQ('1',   group_str[5]);
        EXPECT_EQ('4',   group_str[6]);
        EXPECT_EQ('8',   group_str[7]);
        EXPECT_EQ('\0',  group_str[8]);
}

TEST_F(tf_tag_mesg_parser, get_tag_group_part_correct_num_1) {
	char mesg_str[] = "02:06:CAT=(34)";
	char tag_str[] = "CAT";
	char group_part_str[10];
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_group_part(group_part_str, sizeof(group_part_str), 0);
	EXPECT_EQ(true, ret);
        EXPECT_EQ('3',   group_part_str[0]);
        EXPECT_EQ('4',   group_part_str[1]);
        EXPECT_EQ('\0',  group_part_str[2]);
}

TEST_F(tf_tag_mesg_parser, get_tag_group_part_correct_num_2) {
	char mesg_str[] = "02:06:CAT=(34,62)";
	char tag_str[] = "CAT";
	char group_part_str[10];
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_group_part(group_part_str, sizeof(group_part_str), 1);
	EXPECT_EQ(true, ret);
        EXPECT_EQ('6',   group_part_str[0]);
        EXPECT_EQ('2',   group_part_str[1]);
        EXPECT_EQ('\0',  group_part_str[2]);
}

TEST_F(tf_tag_mesg_parser, get_tag_group_part_as_int_1) {
	char mesg_str[] = "02:06:CAT=(12456)";
	char tag_str[] = "CAT";
	char group_part_str[10];
	int value = 0;
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_group_part_as_int(&value, 0);
	EXPECT_EQ(true, ret);
        EXPECT_EQ(12456,  value);
}

TEST_F(tf_tag_mesg_parser, get_tag_group_part_as_int_2) {
	char mesg_str[] = "02:06:CAT=(2147483647)";
	char tag_str[] = "CAT";
	char group_part_str[10];
	int value = 0;
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_group_part_as_int(&value, 0);
	EXPECT_EQ(true, ret);
        EXPECT_EQ(2147483647, value);
}

TEST_F(tf_tag_mesg_parser, get_tag_group_part_as_int_3) {
	char mesg_str[] = "02:06:CAT=(-2147483648)";
	char tag_str[] = "CAT";
	char group_part_str[10];
	int value = 0;
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.get_tag_group_part_as_int(&value, 0);
	EXPECT_EQ(true, ret);
        EXPECT_EQ(-2147483648, value);
}

TEST_F(tf_tag_mesg_parser, compare_tag_at_offset_normal) {
	char mesg_str[] = "DIRECT To=06";
	char tag_str[] = "DIRECT";
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.compare_tag_at_offset(0);
	EXPECT_EQ(true, ret);
}

TEST_F(tf_tag_mesg_parser, compare_tag_at_offset_normal_fail) {
	char mesg_str[] = "CAT To=06";
	char tag_str[] = "DIRECT";
	tag_parser.load_mesg_str(mesg_str, sizeof(mesg_str));
	tag_parser.load_tag_str(tag_str, sizeof(tag_str));
	bool ret =  tag_parser.compare_tag_at_offset(0);
	EXPECT_EQ(false, ret);
}


}  // namespace
